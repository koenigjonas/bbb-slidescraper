# BBB-Slidescraper

The little Python script can scrape all your BBB (Big Blue Button) slides and convert them to a PDF


# What do you need
1. Python 3
2. pip3
### The following pip3 packages
#### cairosvg
```
pip3 install cairosvg
```
#### PyPDF2
```
pip3 install PyPDF2
```
#### wget
```
pip3 install wget
```
# How to use this:
if you cloned the project and installed all the required stuff go to your BBB room,
rightclick the presentation and click copy the image link.
Then your have a link like: "https://your.bbb.room/longstringoffstuff/svg/10".
remove the last number completly and copy the URL.
Now you can all the propram:
```
python3 bbb-scraper.py -i https://your.bbb.room/longstringoffstuff/svg/ -o output.pdf -c 43

```
where -i is the input URL, -o is the output file and -c is the count of slides
