
import cairosvg
import PyPDF2
import wget
import os
import sys, getopt
#'https://lecture06.uni-leipzig.de/bigbluebutton/presentation/1503246a454225bac079c39718b25033faf98f6a-1621406775007/1503246a454225bac079c39718b25033faf98f6a-1621406775007/35fab0f4d60468e53bd6a3df728071acb03c0635-1621407229592/svg/'+str(i)



def main(argv):
   url = ''
   outputfile = ''
   countofslides = 0
   try:
      opts, args = getopt.getopt(argv,"hi:o:c:",["ifile=","ofile=","slidecount="])
   except getopt.GetoptError:
      print('test.py -i <inputfile> -o <outputfile> -c <slidecount>')
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('test.py -i <url> -o <outputfile> -c <countofslides>')
         sys.exit()
      elif opt in ("-i", "--ifile"):
         url = arg
      elif opt in ("-o", "--ofile"):
         outputfile = arg
     elif opt in("-c", "--slidecount"):
         countofslides = int(arg)+1
   print('Input file is "', url)
   print('Output file is "', outputfile)
   print('There are '+str(countofslides-1)+' slides')
   mergeFile = PyPDF2.PdfFileMerger()
   os.mkdir("./.tmp")
   for i in range(1,countofslides):
       wget.download(url+str(i),'./.tmp/out'+str(i)+'.svg')
       cairosvg.svg2pdf(file_obj=open('./.tmp/out'+str(i)+'.svg', "rb"), write_to='./.tmp/out'+str(i)+'.pdf')
       mergeFile.append('./.tmp/out'+str(i)+'.pdf',"rb")
   mergeFile.write(outputfile)
   for root, dirs, files in os.walk("./.tmp"):
       for file in files:
           os.remove(os.path.join(root, file))
   os.rmdir("./.tmp")
if __name__ == "__main__":
   main(sys.argv[1:])
